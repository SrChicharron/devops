// {{self.addEventListener('fetch',event=>{
// const ofLineResponse= new Response(`
// <!DOCTYPE html>
// <html lang="en">
//   <head>
//   <title>React App</title>
//   </head>
//   <body>
//   <h1>Offline Mode</h1>
//   </body>
// </html>
// `,{
//     headers:{
//     'Content-Type':'text/html'
//     }
// })
// const response=fetch(event.request)
// .catch(() =>ofLineResponse)

// event.respondWith(response)

// })}}

const CACHE_NAME='CACHE.5'
const CACHE_INMUTABLE='inmutable-v1';
const CACHE_DYNAMIC='dynamic-v1';
const CACHE_STATIC='static-v2';

const limpiarCache = (cacheName, numeroItems) =>{
  caches.open(cacheName).then(cache=>{
    cache.keys().then(keys=>{
      if(keys.length > numeroItems){
        cache.delete(keys[0]).then(limpiarCache(cacheName, numeroItems))
      }
    })
  })
}

self.addEventListener('install', function(event){

  const cacheStatic = caches.open(CACHE_STATIC).then(function(cache){
  return cache.addAll([
    '/',
    '/index.html',
    '/js/app.js',
    '/sw.js',
    '/static/js/bundle.js',
    'favicon.ico',
    'logo512.png',
    'logo192.png',

  ])
  })

const cacheInmutable = caches.open(CACHE_INMUTABLE).then(function(cache){
  return cache.addAll([
    'https://fonts.googleapis.com/css2?family=Pixelify+Sans&display=swap'
  ])
})
 event.waitUntil(cacheStatic, cacheInmutable)
})

// self.addEventListener('fetch',function (event){
// // Cache only, todo es servido desde el cache
//  event.respondWith(caches.match(event.request))
// })


//self.addEventListener('fetch',function (event){
  // Cache network fllback
//  const  respuesta= caches.match(event.request)
//  .then(response=>{
//  if(response) return response
  //Si no existe
//  return fetch(event.request)
//  .then(newResponse=>{
//
//    caches.open(CACHE_NAME)
//    .then(cache=>{
//      cache.put(event.request,newResponse)
//    }
//      )
//
//  return newResponse.clone()
//  }
//    )
//  })
//   event.respondWith(respuesta)
//  })


// Esta cache primero pregunta a internet y despues a cache.
self.addEventListener('fetch', function (event) {
  event.respondWith(
      fetch(event.request)
          .then(networkResponse => {
              // Se almacena en la CACHE_DYNAMIC
              caches.open(CACHE_DYNAMIC)
                  .then(cache => {
                      cache.put(event.request, networkResponse);
                      limpiarCache(CACHE_DYNAMIC, 2);
                  });

              // respueta
              return networkResponse.clone();
          })
          .catch(() => {
              // En caso de qeu se vaya a la chache
              return caches.match(event.request)
                  .then(cachedResponse => {
                      if (cachedResponse) {
                          // Se devueleve el recurso
                          return cachedResponse;
                      } else {
                          // Rpuesta fallida en caso de que o exista
                          return new Response('No hay conexión a Internet');
                      }
                  });
       })
);
});